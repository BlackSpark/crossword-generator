import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Creator: jonak_000
 * Date: 10/23/2015
 * Time: 10:06 PM
 */
public class Util
{

    public static String stringify2DString(String[][] stringGrid)
    {
        int width;
        StringBuilder grid = new StringBuilder();
        if(stringGrid.length > 0) {
            width = stringGrid[0].length;
        } else {
            return "";
        }

        for(String[] row : stringGrid) {
            for(int i = 0; i < width; i++) {
                grid.append(row[i]);
                grid.append(" ");
            }
            grid.append("\n");
        }

        return grid.toString();
    }

    public static String reverseString(String str)
    { // http://stackoverflow.com/questions/20312214/reverse-string-method
        String reverse = "";
        int length = str.length();
        for(int i = length - 1; i >= 0; i--) {
            reverse = reverse + str.charAt(i);
        }
        return reverse;
    }

    public static TreeMap<String, String> getVocabMap()
    {
        TreeMap<String, String> vocab = new TreeMap<>();
        InputStream is = ClassLoader.getSystemResourceAsStream("words.txt");
        Reader fileReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        Scanner vocabFile = new Scanner(fileReader);

        while(vocabFile.hasNextLine()) {
            String line = vocabFile.nextLine();
            if(line.contains(",")) {
                String[] split = vocabFile.nextLine().split(",");
                vocab.put(split[0], split[1]);
            } else {
                vocab.put(line, line);
            }
        }

        return vocab;
    }

    public static String[] getKeyArray(TreeMap<String, String> a)
    {
        Object[] keys = a.keySet().toArray();
        String[] retValue = new String[keys.length];
        int i = 0;

        for(Object key : keys) {
            retValue[i] = (String) key;
            i++;
        }

        return retValue;
    }

}
