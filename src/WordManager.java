import java.util.Random;

/**
 * Creator: jonak_000
 * Date: 10/23/2015
 * Time: 10:09 PM
 */
public class WordManager
{


    public static String[] wordsToUse(int num, String[] vocab) // Gets the words to use from the array. num = number of words to pick.
    {
        if(num >= vocab.length) {
            return new String[]{"Invalid"};
        }
        Random R = new Random(); // Random number generator
        String[] words = new String[num]; // The chosen words
        int[] alreadyChosen = new int[num]; // Stores the indexes of words already chosen so we don't pick them again.

        for(int i = 0; i < num; i++) { // For all the words we need to generate
            int randomIndex; // Init here because of scope
            while(true) {
                boolean useThisIndex = true; // We assume that the word wasn't already chosen
                randomIndex = R.nextInt(vocab.length); // Set the random index to a random int
                for(int number : alreadyChosen) {
                    if(number == randomIndex) {
                        useThisIndex = false; // Iterate through the already chosen words and if it's already been chosen then try again
                    }
                }
                if(useThisIndex)
                    break; // If the word's good, continue
            }
            words[i] = vocab[randomIndex]; // Cool, so we got a good word. Get it saved...
            alreadyChosen[i] = randomIndex; // ...and make sure we don't choose it again.
        }

        return words;
    }

    public static String[] makeSuitableForCrossword(String[] words)
    { // Makes all words uppercase and removes non-word characters.
        String[] suitableWords = new String[words.length];

        for(int i = 0; i < words.length; i++) {
            suitableWords[i] = words[i].toUpperCase().replaceAll("[-'\"\\\\/.,!? ]", "");
        }

        return suitableWords;
    }

    public static int getLongestWord(String[] words)
    { // Gets the longest word in a string array
        int longest = 0;

        for(String word : words) {
            if(word.length() > longest) { // If the length is more than the longest
                longest = word.length(); // Then that must be longer!
            }
        }

        return longest;
    }
}
