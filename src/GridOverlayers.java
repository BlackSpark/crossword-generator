/**
 * Creator: jonak_000
 * Date: 10/23/2015
 * Time: 10:14 PM
 */
public class GridOverlayers
{
    public static Object[] LR(String word, String[][] grid, boolean[][] marker, int gridLength, int x, int y)
    {
        if(x + word.length() > gridLength) {
            return null;
        }
        int ii = 0;
        for(int i = x; i < x + word.length(); i++) {
            if(marker[y][i] || word.substring(ii, ii + 1).equals(grid[y][i])) {
                return null;
            }
            ii++;
        }
        ii = 0;
        for(int i = x; i < x + word.length(); i++) {
            grid[y][i] = word.substring(ii, ii + 1);
            marker[y][i] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    public static Object[] RL(String word, String[][] grid, boolean[][] marker, int gridLength, int x, int y)
    {
        word = Util.reverseString(word);
        if(x + word.length() > gridLength) {
            return null;
        }
        int ii = 0;
        for(int i = x; i < x + word.length(); i++) {
            if(marker[y][i] || word.substring(ii, ii + 1).equals(grid[y][i])) {
                return null;
            }
            ii++;
        }
        ii = 0;
        for(int i = x; i < x + word.length(); i++) {
            grid[y][i] = word.substring(ii, ii + 1);
            marker[y][i] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    public static Object[] UD(String word, String[][] grid, boolean[][] marker, int gridLength, int y)
    {
        if(y + word.length() > gridLength) {
            return null;
        }
        int ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            if(marker[i][y] || word.substring(ii, ii + 1).equals(grid[y][i])) {
                return null;
            }
        }
        ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            grid[i][y] = word.substring(ii, ii + 1);
            marker[i][y] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    public static Object[] DU(String word, String[][] grid, boolean[][] marker, int gridLength, int y)
    {
        word = Util.reverseString(word);
        if(y + word.length() > gridLength) {
            return null;
        }
        int ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            if(marker[i][y] || word.substring(ii, ii + 1).equals(grid[y][i])) {
                return null;
            }
        }
        ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            grid[i][y] = word.substring(ii, ii + 1);
            marker[i][y] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    public static Object[] DTLBR(String word, String[][] grid, boolean[][] marker, int gridLength, int y)
    {
        if(y + word.length() > gridLength) {
            return null;
        }
        int ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            if(marker[i][i] || word.substring(ii, ii + 1).equals(grid[i][i])) {
                return null;
            }
        }
        ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            grid[i][i] = word.substring(ii, ii + 1);
            marker[i][i] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    public static Object[] DBRTL(String word, String[][] grid, boolean[][] marker, int gridLength, int y)
    {
        word = Util.reverseString(word);
        if(y + word.length() > gridLength) {
            return null;
        }
        int ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            if(marker[i][i] || word.substring(ii, ii + 1).equals(grid[i][i])) {
                return null;
            }
        }
        ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            grid[i][i] = word.substring(ii, ii + 1);
            marker[i][i] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    @Deprecated
    public static Object[] DTRBL(String word, String[][] grid, boolean[][] marker, int gridLength, int x, int y)
    {
        x = gridLength - 1 - x;
        int temp6 = x - word.length();
        if(y + word.length() > gridLength && temp6 < 0) {
            return null;
        }
        int ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            if(marker[i][x] || word.substring(ii, ii + 1).equals(grid[i][x])) {
                return null;
            }
            x--;
        }
        ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            grid[y][y] = word.substring(ii, ii + 1);
            marker[y][y] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }

    @Deprecated
    public static Object[] DBLTR(String word, String[][] grid, boolean[][] marker, int gridLength, int x, int y)
    {

        word = Util.reverseString(word);
        x = gridLength - x;
        if(y + word.length() > gridLength && x - word.length() < 0) {
            return null;
        }
        int ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            if(marker[i][x] || word.substring(ii, ii + 1).equals(grid[i][x])) {
                return null;
            }
            x--;
        }
        ii = 0;
        for(int i = y; i < y + word.length(); i++) {
            grid[y][y] = word.substring(ii, ii + 1);
            marker[y][y] = true;
            ii++;
        }

        return new Object[]{grid, marker};

    }
}

