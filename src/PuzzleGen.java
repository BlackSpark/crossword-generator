import java.util.Random;
import java.util.TreeMap;

/**
 * Creator: jonak_000
 * Date: 10/23/2015
 * Time: 10:11 PM
 */
public class PuzzleGen
{
    private static String word;
    private static String[][] grid;
    private static boolean[][] marker;
    private static int tries;
    private static int successes;
    private static int gridLength;

    public static String setWordsInCrosswordPuzzle(TreeMap<String, String> vocabWords, String[] rawWords, String letterGridP, int gridLengthP)
    { // The meat of the app. Takes a random grid and overlays it with words.
        String[] words = WordManager.makeSuitableForCrossword(rawWords);
        gridLength = gridLengthP;

        int maxWordsIndex = words.length - 1;
        Random R = new Random();
        grid = new String[gridLength][gridLength];
        marker = new boolean[gridLength][gridLength];

        String[] rows = letterGridP.split("\\n");
        String[] englishWords = new String[words.length];

        for(int i = 0; i < rawWords.length; i++) {
            englishWords[i] = vocabWords.get(rawWords[i]);
        }

        for(int i = 0; i < gridLength; i++) {
            for(int ii = 0; ii < gridLength; ii++) {
                grid[i][ii] = rows[i].substring(ii, ii + 1);
                marker[i][ii] = false;
            }
        }

        tries = 0;
        successes = 0;

        while(tries < 10000000) {

            word = words[successes];

            int direction = R.nextInt(6);
                /*
                 * 0 - L>R
                 * 1 - R>L
                 * 2 - U>D
                 * 3 - D>U
                 * 4 - Top-L>Bottom-R
                 * 5 - Bottom-R>Top-L
                 * TODO 6 - Top-R>Bottom-L
                 * TODO 7 - Bottom-L>Top-R
                 */
            int x;
            int y;
            switch(direction) {
                case 0:
                    x = R.nextInt(gridLength - word.length());
                    y = R.nextInt(gridLength);
                    break;
                case 1:
                    x = R.nextInt(gridLength - word.length());
                    y = R.nextInt(gridLength);
                    break;
                case 2:
                    x = R.nextInt(gridLength);
                    y = R.nextInt(gridLength - word.length());
                    break;
                case 3:
                    x = R.nextInt(gridLength);
                    y = R.nextInt(gridLength - word.length());
                    break;
                case 4:
                    x = R.nextInt(gridLength - word.length());
                    y = R.nextInt(gridLength - word.length());
                    break;
                case 5:
                    x = R.nextInt(gridLength - word.length());
                    y = R.nextInt(gridLength - word.length());
                    break;
                default:
                    x = R.nextInt(gridLength);
                    y = R.nextInt(gridLength);
                    break;
            }


            directionParse(direction, x, y);

            if(successes > maxWordsIndex)
                break;
        }


        StringBuilder finalTouches = new StringBuilder(Util.stringify2DString(grid));

        int lastIndex = 0;
        int i = 0;
        while(lastIndex != -1 && i < successes) {
            lastIndex = finalTouches.toString().indexOf("\n", lastIndex);

            finalTouches.insert(lastIndex, "\t\t" + englishWords[i]);

            lastIndex += 25;
            i++;
        }

        return finalTouches.toString();

    }


    private static boolean directionParse(int direction, int x, int y)
    {
        Object[] res;
        switch(direction) {
            case 0:
                res = GridOverlayers.LR(word, grid, marker, gridLength, x, y);
                if(res == null) {
                    tries++;
                } else {
                    grid = (String[][]) res[0];
                    marker = (boolean[][]) res[1];
                    successes++;
                    return true;
                }

            case 1:
                res = GridOverlayers.RL(word, grid, marker, gridLength, x, y);
                if(res == null) {
                    tries++;
                } else {
                    grid = (String[][]) res[0];
                    marker = (boolean[][]) res[1];
                    successes++;
                    return true;
                }

            case 2:
                res = GridOverlayers.UD(word, grid, marker, gridLength, y);
                if(res == null) {
                    tries++;
                } else {
                    grid = (String[][]) res[0];
                    marker = (boolean[][]) res[1];
                    successes++;
                    return true;
                }

            case 3:
                res = GridOverlayers.DU(word, grid, marker, gridLength, y);
                if(res == null) {
                    tries++;
                } else {
                    grid = (String[][]) res[0];
                    marker = (boolean[][]) res[1];
                    successes++;
                    return true;
                }

            case 4:
                res = GridOverlayers.DTLBR(word, grid, marker, gridLength, y);
                if(res == null) {
                    tries++;
                } else {
                    grid = (String[][]) res[0];
                    marker = (boolean[][]) res[1];
                    successes++;
                    return true;
                }

            case 5:
                res = GridOverlayers.DBRTL(word, grid, marker, gridLength, y);
                if(res == null) {
                    tries++;
                } else {
                    grid = (String[][]) res[0];
                    marker = (boolean[][]) res[1];
                    successes++;
                    return true;
                }

        }
        return false;
    }

    public static String constructRandomLetterGrid(int maxLength)
    { // Constructs a grid of random letters of length maxLength by maxLength.
        maxLength++;
        Random R = new Random();
        StringBuilder crosswordPuzzle = new StringBuilder();
        int numOfCharacters = (int) Math.pow(maxLength, 2); // The number of individual characters in the grid.

        for(int i = 0; i < numOfCharacters; i++) { // The random character generator thing.
            int randomCharNum = R.nextInt(32); // Generates a random number between 0 and 31
            char randomChar; // Declare a random character

            if(randomCharNum > 25) { // If we're over 25, we've reached special territory
                randomCharNum -= 25; // Subtract 25 because I'm lazy and want to work with numbers 1-6
                switch(randomCharNum) {
                    case 1:
                        randomChar = '\u00C1'; // Á
                        break;
                    case 2:
                        randomChar = '\u00C9'; // É
                        break;
                    case 3:
                        randomChar = '\u00CD'; // Í
                        break;
                    case 4:
                        randomChar = '\u00D3'; // Ó
                        break;
                    case 5:
                        randomChar = '\u00DA'; // Ú
                        break;
                    case 6:
                        randomChar = '\u00D1'; // Ñ
                        break;
                    default:
                        randomChar = 'A'; // The compiler complains if I don't have this.
                }
            } else {
                randomChar = (char) (randomCharNum + 65); // Otherwise, take the random number and add 65, then cast it to char.
                // This turns it to a capital letter A through Z.
            }

            crosswordPuzzle.append(randomChar); // Add the random character to the grid
            if(i % maxLength == 0) { // If there's already (whatever the length is) in a row, add a newline too.
                crosswordPuzzle.append("\n");
            }
            if(i == numOfCharacters - 1) { // If we're on the last character
                randomCharNum = R.nextInt(26) + 65;
                randomChar = (char) randomCharNum;
                crosswordPuzzle.append(randomChar); // .... finish up with one last one!
            }
        }
        crosswordPuzzle.replace(0, 2, ""); // Remove an extra newline and character.

        return crosswordPuzzle.toString().trim(); // Return the random character grid
    }

}
