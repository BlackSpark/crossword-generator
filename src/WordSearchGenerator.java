import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Creator: jonak_000
 * Date: 10/22/2015
 * Time: 5:00 PM
 */

public class WordSearchGenerator
{

    private static final Scanner in = new Scanner(System.in); // Scanner for user input
    private static TreeMap<String, String> vocabWords;
    private static PrintWriter pw; // PrintWriter so that we can print to a file

    public static void main(String[] args)
    {
        try { // Get a new file so we can output the puzzles.
            File f = new File("Generated Crossword Puzzles.txt");
            f.delete();
            pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream("Generated Crossword Puzzles.txt"), StandardCharsets.UTF_8));
        } catch(FileNotFoundException e) {
            System.out.println("Cannot write to file 'Generated Crossword Puzzles.txt'. Exiting...");
            System.exit(-2);
        }

        vocabWords = Util.getVocabMap();

        System.out.println("How many puzzles would you like to generate?");
        int numPuzzles = 5; // Default = 5
        try {
            numPuzzles = in.nextInt();
            if(numPuzzles < 1) {
                System.out.println("Please enter a number 1 or more. Defaulting to 5.");
            }
        } catch(InputMismatchException e) {
            System.out.println("Invalid input. Generating 5.");
        }

        System.out.println("How many max words per puzzle?");
        int numWordsPerPuzzle = 5; // Default = 5
        try {
            numWordsPerPuzzle = in.nextInt();
            if(numWordsPerPuzzle < 1) {
                System.out.println("Please enter a number 1 or more. Defaulting to 5.");
            }
        } catch(InputMismatchException e) {
            System.out.println("Invalid input. Assuming 5.");
        }

        System.out.println("Enter difficulty level");
        int difficultyLevel = 1; // Default = 1
        try {
            difficultyLevel = in.nextInt();
            if(difficultyLevel < 1) {
                System.out.println("Please enter a number 1 or more. Defaulting to 1.");
            }
        } catch(InputMismatchException e) {
            System.out.println("Invalid input. Assuming 1.");
        }

        for(int i = 0; i < numPuzzles; i++) { // Generates however many puzzles the user requested, and prints them to the file.
            String[] words = WordManager.wordsToUse(numWordsPerPuzzle, Util.getKeyArray(vocabWords));
            int gridSize = WordManager.getLongestWord(words) + difficultyLevel;
            String puzzle = PuzzleGen.setWordsInCrosswordPuzzle(vocabWords, words, PuzzleGen.constructRandomLetterGrid(gridSize), gridSize);

            pw.println(puzzle);

            System.out.println(i + 1 + " Done");
        }

        pw.close(); // Close the file so that other apps can use it.

        System.out.println("The crossword puzzles have been generated.");
        System.out.println("Look in the file 'Generated Crossword Puzzles.txt'");

    }

}
